//
//  draww.m
//  BeizerLine
//
//  Created by Hadi Hatunoglu on 19/09/13.
//  Copyright (c) 2013 Abilash Reddy Kallepu. All rights reserved.
//

#import "draww.h"

@implementation draww

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
 
 
 UIBezierPath *path=[UIBezierPath bezierPath];
 path.lineWidth=5.0;
 [[UIColor blackColor] setStroke];
 [path moveToPoint:CGPointMake(100,100)];
 [path addLineToPoint:CGPointMake(200,200)];
 [path addLineToPoint:CGPointMake(300,300)];
 [path stroke];
    NSLog(@"log added");
    // Drawing code
}


@end
