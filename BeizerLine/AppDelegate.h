//
//  AppDelegate.h
//  BeizerLine
//
//  Created by Hadi Hatunoglu on 19/09/13.
//  Copyright (c) 2013 Abilash Reddy Kallepu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
